##
## Makefile for  in /home/grandr_r/efi/Myre
##
## Made by grandr_r
## Login   <grandr_r@padawan.ru>
##
## Started on  Thu Feb 16 09:38:55 2017 grandr_r
## Last update Tue Jul 25 10:39:03 2017 grandr_r
##

SRC=	src/main.c \
	src/string_utils.c \
	src/efi_utils.c \
	src/shell.c \
	src/keyboard.c \
	src/commands.c \
	src/array_utils.c \
	src/line_utils.c \
	src/env.c \
	src/read_config.c \
	src/graphics.c \
	src/load_bmp.c \
	src/picopng.c \
	src/entries.c \
	src/images.c \
	src/list.c \
	src/folder_utils.c \
	src/autoscan.c \
	src/linked_list.c \
	src/io.c \
	gnu-efi/data.c \
	gnu-efi/print.c \
	gnu-efi/callwrap.c \
	gnu-efi/boxdraw.c \
        gnu-efi/cmdline.c \
        gnu-efi/console.c \
        gnu-efi/crc.c \
        gnu-efi/debug.c \
        gnu-efi/dpath.c \
        gnu-efi/error.c \
        gnu-efi/event.c \
        gnu-efi/guid.c \
        gnu-efi/hand.c \
        gnu-efi/hw.c \
        gnu-efi/init.c \
        gnu-efi/initplat.c \
        gnu-efi/lock.c \
        gnu-efi/math.c \
        gnu-efi/misc.c \
        gnu-efi/smbios.c \
        gnu-efi/sread.c \
        gnu-efi/str.c \
	gnu-efi/efirtlib.c \
	gnu-efi/rtdata.c \
	gnu-efi/rtlock.c \
	gnu-efi/rtstr.c \
	gnu-efi/vm.c

OBJ=		$(SRC:.c=.o)

NAME=		efi/BOOTX64.efi

%.o:		%.c
		x86_64-w64-mingw32-gcc -Werror=implicit-fallthrough=0 -ffreestanding -I./gnu-efi/inc/ -I./gnu-efi/inc/x86_64 -I./gnu-efi/inc/protocol -I./include -Wall -Wextra -Werror -DEFI_FUNCTION_WRAPPER -DGNU_EFI_USE_MS_ABI -c -o $@ $<

all:		$(NAME)

$(NAME):	$(OBJ)
		x86_64-w64-mingw32-gcc -nostdlib -Wl,-dll -shared -Wl,--subsystem,10 -e efi_main -o $(NAME) $(OBJ) -lgcc

clean:
		rm -f $(OBJ)

fclean:
		rm -f $(OBJ) *~ include/*~ src/*~ $(NAME)
		rm -f qemu-fs/BOOTX64.efi qemu-fs/Myre.cfg

re:		fclean all
