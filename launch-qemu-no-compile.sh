#!/bin/sh

qemu-system-x86_64 -m 512 -M q35 -nodefaults -vga std -drive if=pflash,format=raw,readonly,file=./qemu-env/OVMF_CODE.fd -drive file=fat:rw:.,media=disk,if=virtio,format=raw
