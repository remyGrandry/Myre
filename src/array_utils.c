/*
** array_utils.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Feb 21 15:14:54 2017 grandr_r
** Last update Wed Jul 26 08:32:18 2017 grandr_r
*/

#include <Myre.h>

void		swap_array(CHAR16 **arr, CHAR16 **arr2)
{
  CHAR16	*tmp;

  tmp = *arr;
  *arr = *arr2;
  *arr2 = tmp;
}

void		sort_array(CHAR16 **arr)
{
  int		i;

  i = 1;
  while (arr[i])
    {
      if (!arr[i])
        i = 1;
      if (strcmp(arr[i], arr[i - 1]) < 0)
	swap_array(&arr[i - 1], &arr[i]);
      i++;
    }
}

void		copy_array(CHAR16 **dest, CHAR16 **src)
{
  UINT8		i;

  i = 0;
  while (src[i])
    {
      dest[i] = strdup(src[i]);
      i++;
    }
  dest[i] = NULL;
  dest[i + 1] = NULL;
}


CHAR16		**realloc_array(CHAR16 **arr, UINT8 size)
{
  CHAR16	**ret;

  ret = AllocatePool((size + 1) * sizeof(CHAR16 *));
  copy_array(ret, arr);
  free_array(arr);
  return (ret);
}

int	arraylen(CHAR16 **arr)
{
  int	i;

  i = 0;
  if (arr == NULL)
    return (0);
  while (arr[i])
    i++;
  return (i);
}

UINTN	array_memory_space(CHAR16 **arr)
{
  int	i;
  UINTN	total_space;

  i = 0;
  total_space = 0;
  while (arr[i])
    {
      total_space += strlen16(arr[i]);
      i++;
    }
  return (total_space);
}

void	show_array(CHAR16 **arr)
{
  int	i;

  i = 0;
  while (arr[i])
    {
      if (arr[i][0] != 0)
	Print(L"%s\n", arr[i]);
      i++;
    }
}

void	free_array(CHAR16 **arr)
{
  int	i;

  i = 0;
  while (arr[i])
    {
      FreePool(arr[i]);
      i++;
    }
  FreePool(arr);
}

int     count_word(CHAR16 *str, char sep)
{
  int   i;
  int   nbsep;

  i = 0;
  nbsep = 0;
  while (str[i])
    {
      if (str[i] == sep)
	{
          while (str[i] == sep)
            i++;
          nbsep++;
        }
      i++;
    }
  return (nbsep + 1);
}

int     word_lenght(CHAR16 *str, char sep)
{
  int   lenght;

  lenght = 0;
  while (str[lenght] != sep && str[lenght])
    lenght++;
  return (lenght);
}

CHAR16		**str_to_array(CHAR16 *str, char sep)
{
  CHAR16 	**ret;
  CHAR16	*epured_str;
  int 		i;
  int   	j;
  int		nb_words;
  int		cur_lenght;

  i = 0;
  j = 0;
  epured_str = epur_str(str, ' ');
  nb_words = count_word(epured_str, sep);
  ret = AllocatePool(sizeof(CHAR16 *) * (nb_words + 1));
  while (i < nb_words)
    {
      cur_lenght = word_lenght(&epured_str[j], sep);
      ret[i] = AllocatePool(sizeof(CHAR16) * (cur_lenght + 1));
      memset(ret[i], 0, cur_lenght + 1);
      strncpy(ret[i], &epured_str[j], word_lenght(&epured_str[j], sep));
      j += cur_lenght + 1;
      while (epured_str[j] == sep && epured_str[j])
	j++;
      i++;
    }
  ret[i] = NULL;
  FreePool(epured_str);
  return (ret);
}
