/*
** autoscan.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Apr 18 07:57:12 2017 grandr_r
** Last update Thu Apr 27 13:27:38 2017 grandr_r
*/

#include <Myre.h>

// TODO : FIND A BETTER SOLUTION : QUICK !!!!!!

CHAR16		*path_arr[5] = {L"*\\Microsoft\\Boot\\bootmgfw.efi", L"*\\Clover\\CLOVERX64.efi", L"*\\refind\\BOOTX64.efi", L"*\\grubx64.efi", NULL};
CHAR16		*name_arr[5] = {L"Windows", L"Clover", L"Refind", L"Linux", L"Unknow"};
CHAR16		*icon_arr[5] = {L"\\EFI\\Myre\\images\\windows-generic.png", L"\\EFI\\Myre\\images\\clover-generic.png", L"\\EFI\\Myre\\images\\refind-generic.png", L"\\EFI\\Myre\\images\\linux-generic.png", L"\\EFI\\Myre\\images\\unknow-generic.png"};


CHAR16			*get_this_entry_name(CHAR16 *path)
{
  UINT8			i;

  i = 0;
  while (path_arr[i])
    {
      if (match_case(path, path_arr[i]) >= 1)
	return (name_arr[i]);
      i++;
    }
  return (NULL);
}

CHAR16			*get_this_entry_icon(CHAR16 *path)
{
  UINT8			i;

  i = 0;
  while (path_arr[i])
    {
      if (match_case(path, path_arr[i]) >= 1)
	return (icon_arr[i]);
      i++;
    }
  return (NULL);
}

VOID			get_alloc_sizes(t_link *list, UINTN *entries_path_size, UINTN *entries_icon_size)
{
  t_link	        *tmp;

  *entries_icon_size = 0;
  *entries_path_size = 0;
  tmp = list;
  while (tmp->prev != NULL)
    tmp = tmp->prev;
  while (tmp->next != NULL)
    {
      *entries_path_size += strlen(tmp->data) + strlen(get_this_entry_name(tmp->data)) + 15;
      *entries_icon_size += strlen(get_this_entry_icon(tmp->data)) + 4;
      tmp = tmp->next;
    }
}

VOID			get_full_list(t_link *list, CHAR16 **entries_path, CHAR16 **entries_icon)
{
  t_link	        *tmp;

  tmp = list;
  while (tmp->prev != NULL)
    tmp = tmp->prev;
  while (tmp->next != NULL)
    {
      strcat(*entries_path, get_this_entry_name(tmp->data));
      strcat(*entries_path, L":");
      strcat(*entries_path, tmp->data);
      strcat(*entries_path, L"|");
      strcat(*entries_icon, get_this_entry_icon(tmp->data));
      strcat(*entries_icon, L"|");
      tmp = tmp->next;
    }
  (*entries_path)[strlen(*entries_path) - 1] = ' ';
  (*entries_icon)[strlen(*entries_icon) - 1] = ' ';
}

VOID			add_entries_env(t_env *env, CHAR16 *var_name, CHAR16 *entries_list)
{
  CHAR16		*var_to_add;

  var_to_add = AllocateZeroPool(sizeof(CHAR16) * (strlen(var_name) + strlen(entries_list) + 2));
  strcpy(var_to_add, var_name);
  strcat(var_to_add, entries_list);
  add_to_env(env, var_to_add);
  FreePool(var_to_add);
}

VOID			list_to_env(t_link *list, t_env *env)
{
  UINTN			entries_path_size;
  UINTN			entries_icon_size;
  CHAR16		*entries_path;
  CHAR16		*entries_icon;

  get_alloc_sizes(list, &entries_path_size, &entries_icon_size);
  if (entries_icon_size == 0 && entries_icon_size == 0)
    return;
  entries_path = AllocateZeroPool(entries_path_size * sizeof(CHAR16));
  entries_icon = AllocateZeroPool(entries_icon_size * sizeof(CHAR16));
  get_full_list(list, &entries_path, &entries_icon);
  add_entries_env(env, L"ENTRY_LIST=", entries_path);
  add_entries_env(env, L"ICON_LIST=", entries_icon);
  //Print(L"Entries list : %s\r\n", entries_path);
  //Print(L"Entries icons : %s\r\n", entries_icon);
  FreePool(entries_path);
  FreePool(entries_icon);
  (void)env;
}

//TODO : SPLIT THIS FUNCTION
VOID			scan_all(t_link *list, CHAR16 *path, t_env *env)
{
  UINTN                 size;
  VOID                  *buffer;
  EFI_FILE_HANDLE	currdir;
  EFI_FILE_INFO         *info;
  EFI_STATUS            stat;
  UINT8                 end_of_listing;
  CHAR16		*filename;

  end_of_listing = 0;
  stat = env->base_dir->Open(env->base_dir, &currdir, path, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(stat))
    return;
  while (end_of_listing != 1)
    {
      size = 256;
      buffer = AllocatePool(size);
      stat = currdir->Read(currdir, &size, buffer);
      if (EFI_ERROR(stat) || size == 0)
        end_of_listing = 1;
      else
        {
          info = (EFI_FILE_INFO *)buffer;
	  //if it's efi file
	  filename = get_full_path(path, info->FileName);
	  if (get_this_entry_name(filename) != NULL)
	    push_before(list, filename);
	  if (info->Attribute & EFI_FILE_DIRECTORY && info->FileName[0] != '.')
	    scan_all(list, get_full_path(path, info->FileName), env);
	  FreePool(filename);
        }
      FreePool(buffer);
    }
  currdir->Close(currdir);
  FreePool(path);
}

VOID			autoscan_entries(t_env *env)
{
  t_link		*ret;

  log(env, L"Scanning entries : ");
  ret = init_link(L"");
  scan_all(ret, strdup(L"\\"), env);
  log(env, L"done\nAdding them to env :");
  //show_link(ret);
  list_to_env(ret, env);
  log(env, L" done\n");
  free_link(ret);
}
