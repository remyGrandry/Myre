/*
** entries.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Tue Mar 28 09:53:06 2017 grandr_r
** Last update Wed May  3 08:36:00 2017 grandr_r
*/

#include <Myre.h>

CHAR16                    *format_path(CHAR16 *path)
{
  UINT8                   i;

  i = 0;
  while (path[i])
    {
      if (path[i] == '/')
        path[i] = '\\';
      i++;
    }
  return (path);
}

void                    entry_to_env(t_env *env, CHAR16 *entry)
{
  CHAR16                *str;
  UINT8                  size;

  size = strlen16(entry) + strlen16(L"SELECTED_EFI=") + sizeof(CHAR16) * 10;
  str = AllocatePool(size * 2);
  memset(str, 0, size);
  strcpy(str, L"SELECTED_EFI=");
  strcat(str, entry);
  add_to_env(env, str);
  FreePool(str);
}

CHAR16		*get_entry_icon(t_env *env, UINT8 nb_entry)
{
  CHAR16	**var;
  CHAR16	*ret;
  CHAR16	*env_value;

  env_value = strdup(getenv_cmd(env, L"ICON_LIST="));
  var = str_to_array(env_value, '|');
  FreePool(env_value);
  ret = strdup(var[nb_entry]);
  free_array(var);
  return (ret);
}

CHAR16		*get_entry_path(t_env *env, UINT8 i)
{
  CHAR16	**var;
  CHAR16	**splitted_entry;
  CHAR16	*ret;
  CHAR16	*env_value;

  env_value = strdup(getenv_cmd(env, L"ENTRY_LIST="));
  var = str_to_array(env_value, '|');
  FreePool(env_value);
  splitted_entry = str_to_array(var[i], ':');
  ret = strdup(splitted_entry[1]);
  format_path(ret);
  free_array(splitted_entry);
  free_array(var);
  return (ret);
}

CHAR16		*get_entry_name(t_env *env, UINT8 i)
{
  CHAR16	**var;
  CHAR16	**splitted_entry;
  CHAR16	*ret;
  CHAR16	*env_value;

  env_value = strdup(getenv_cmd(env, L"ENTRY_LIST="));
  var = str_to_array(env_value, '|');
  FreePool(env_value);
  splitted_entry = str_to_array(var[i], ':');
  ret = strdup(splitted_entry[0]);
  format_path(ret);
  free_array(splitted_entry);
  free_array(var);
  return (ret);
}

UINT8		get_nb_entry(t_env *env)
{
  CHAR16	**var;
  UINT8		ret;
  CHAR16	*list;

  list = strdup(getenv_cmd(env, L"ENTRY_LIST="));
  var = str_to_array(list, '|');
  ret = arraylen(var);
  free_array(var);
  FreePool(list);
  return (ret);
}
