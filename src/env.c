/*
** env.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Fri Feb 24 08:04:08 2017 grandr_r
** Last update Wed Jul 26 08:02:47 2017 grandr_r
*/

#include <Myre.h>

void	env(t_shell *shell, __attribute__((unused)) CHAR16 **args)
{
  show_array(shell->env->env);
}

CHAR16	*getenv_cmd(t_env *env, CHAR16 *request)
{
  int	i;
  int	len;

  i = 0;
  len = strlen(request);
  while (env->env[i])
    {
      if (strncmp(env->env[i], request, len) == 0)
	return (&env->env[i][len]);
      i++;
    }
  return (NULL);
}

void	getenv(t_shell *shell, CHAR16 **args)
{
  int	i;
  int	len;

  i = 0;
  if (arraylen(args) < 2)
    {
      Print(L"Error : Usage : %s arg_to_get\r\n", args[0]);
      return;
    }
  len = strlen(args[1]);
  Print(L"#%s#\r\n", args[1]);
  while (shell->env->env[i])
    {
      if (strncmp(shell->env->env[i], args[1], len) == 0)
	{
	  Print(L"%s\r\n", &shell->env->env[i][len + 1]);
	  return;
	}
      i++;
    }
  Print(L"%s : not in env\r\n", args[1]);
}

int		verify_env_token(CHAR16 *str)
{
  int		i;
  int		as_equal;

  i = 0;
  as_equal = 0;
  while (str[i])
    {
      if (str[i] == '=')
	as_equal = 1;
      i++;
    }
  if (str[strlen(str) - 1] == '=' || as_equal == 0)
    return (0);
  return (1);
}

int		var_lenght(CHAR16 *str)
{
  int		i;

  i = 0;
  while (str[i] != '=' && str[i])
    i++;
  return (i);
}

void		unset(t_env *env, CHAR16 *str)
{
  int		i;
  int		j;
  CHAR16	**newenv;
  int		len;

  i = 0;
  j = 0;
  len = strlen(str);
  newenv = AllocatePool(sizeof(CHAR16 *) * (arraylen(env->env)));
  while (env->env[i])
    {
      if (strncmp(str, env->env[i], len) != 0)
	newenv[j++] = strdup(env->env[i]);
      i++;
    }
  newenv[j] = NULL;
  free_array(env->env);
  env->env = newenv;
}

void		unsetenv(t_shell *shell, CHAR16 **args)
{
  if (arraylen(args) < 2)
    {
      Print(L"Error : Usage : %s arg_to_remove\r\n", args[0]);
      return;
    }
  if (!getenv_cmd(shell->env, args[1]))
    {
      Print(L"Error : %s: is not a valid environnement variable\r\n", args[1]);
      return;
    }
  unset(shell->env, args[1]);
}

void		add_to_env(t_env *env, CHAR16 *str)
{
  CHAR16	**newenv;
  int		i;
  int		str_is_added;

  i = 0;
  str_is_added = 0;
  newenv = AllocatePool(sizeof(CHAR16 *) * (arraylen(env->env) + 2));
  while (env->env[i])
    {
      if (strncmp(str, env->env[i], var_lenght(env->env[i])) == 0)
	{
	  newenv[i] = strdup(str);
	  str_is_added = 1;
	}
      else
	newenv[i] = strdup(env->env[i]);
      i++;
    }
  if (!str_is_added)
    newenv[i++] = strdup(str);
  newenv[i] = NULL;
  free_array(env->env);
  env->env = newenv;
}

void	setenv(t_shell *shell, CHAR16 **args)
{
  if (arraylen(args) < 2)
    {
      Print(L"Error : Usage : %s arg_to_add\r\n", args[0]);
      return;
    }
  if (verify_env_token(args[1]) == 0)
    {
      Print(L"Error : token is not valid\r\nExemple of syntax : setenv VAR=VALUE\r\n");
      return;
    }
  add_to_env(shell->env, args[1]);
}

void	free_env(t_env *env)
{
  // TODO LEARN HOW TO CLOSE PROTOCOL
  log(env, L"Releasing env ressources\n");
  env->log_file->Close(env->log_file);
  free_array(env->env);
  FreePool(env);
}

void			init_efi_base(t_env *ret, EFI_HANDLE image)
{
  BS->OpenProtocol(image, &LoadedImageProtocol, (VOID **)&ret->loaded_image, image, NULL, EFI_OPEN_PROTOCOL_GET_PROTOCOL);
  ret->base_dir = LibOpenRoot(ret->loaded_image->DeviceHandle);
  ResetKey(&ret->key);
  ret->base_image = image;
  ret->log_file = NULL;
}

void			read_config_and_log(t_env *ret)
{
  EFI_FILE_HANDLE	tmp;

  if (file_exists(ret->base_dir, L"\\EFI\\Myre\\Myre.cfg"))
    {
      if (file_exists(ret->base_dir, L"\\EFI\\Myre\\Myre.log"))
	{
	  ret->base_dir->Open(ret->base_dir, &tmp, L"\\EFI\\Myre\\Myre.log", EFI_FILE_MODE_READ| EFI_FILE_MODE_WRITE, 0);
	  tmp->Delete(tmp);
	}
      ret->base_dir->Open(ret->base_dir, &ret->log_file, L"\\EFI\\Myre\\Myre.log", EFI_FILE_MODE_READ| EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE, 0);
      log(ret, L"Welcome to Myre's log file. Here are various informations about run time events:\n\n*********************************************************************\n");
      read_config(ret, L"\\EFI\\Myre\\Myre.cfg");
      unset(ret, L"CONFIG");
    }
  // ONLY FOR QEMU : TODO REMOVE THIS
  else if (file_exists(ret->base_dir, L"Myre.cfg"))
    {
      if (file_exists(ret->base_dir, L"Myre.log"))
	{
	  ret->base_dir->Open(ret->base_dir, &tmp, L"Myre.log", EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE, 0);
	  tmp->Delete(tmp);
	}
      ret->base_dir->Open(ret->base_dir, &ret->log_file, L"Myre.log", EFI_FILE_MODE_READ| EFI_FILE_MODE_WRITE | EFI_FILE_MODE_CREATE, 0);
      log(ret, L"Welcome to Myre's log file. Here are various informations about run time events:\n\n*********************************************************************\n");
      read_config(ret, L"Myre.cfg");
      unset(ret, L"CONFIG");
    }
}

t_env			*init_env(EFI_HANDLE image)
{
  t_env			*ret;

  ret = AllocatePool(sizeof(*ret));
  init_efi_base(ret, image);
  ret->env = str_to_array(BASE_ENV, ':');
  read_config_and_log(ret);
  if (match_case(getenv_cmd(ret, L"SCAN_ENTRY="), L"true") >= 1)
    autoscan_entries(ret);
  log(ret, L"Verifying configuration :\n");
  verify_config(ret);
  log(ret, L"Configuration verifying done\n");
  return (ret);
}
