/*
** images.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Wed Mar 29 13:51:12 2017 grandr_r
** Last update Fri Apr 14 11:12:42 2017 grandr_r
*/

#include <Myre.h>
#include <picopng.h>

t_img                   *init_image(UINTN height, UINTN width)
{
  t_img                 *ret;

  ret = AllocatePool(sizeof(*ret));
  if (ret == NULL)
    return (NULL);
  ret->height = height;
  ret->width = width;
  ret->data = AllocatePool(height * width * sizeof(EFI_GRAPHICS_OUTPUT_BLT_PIXEL));
  if (ret->data == NULL)
    {
      FreePool(ret);
      return (NULL);
    }
  return (ret);
}

void                    free_image(t_img *image)
{
  FreePool(image->data);
  FreePool(image);
}

t_img                   *read_from_png(t_env *env, CHAR16 *filename)
{
  t_file                *file;
  EFI_FILE_HANDLE       handle;
  EFI_STATUS            err;
  t_img                 *ret;

  err = env->base_dir->Open(env->base_dir, &handle, filename, EFI_FILE_MODE_READ, 0);
  if (EFI_ERROR(err))
    return (NULL);
  file = read_file(handle);
  ret = png_to_img(file);
  FreePool(file->buffer);
  FreePool(file);
  handle->Close(handle);
  return (ret);
}

t_img				*copy_img(t_img *image)
{
  t_img				*ret;
  UINTN				i;

  i = 0;
  ret = init_image(image->height, image->width);
  ret->height = image->height;
  ret->width = image->width;
  while (i < ret->height * ret->width)
    {
      ret->data[i].Red = image->data[i].Red;
      ret->data[i].Green = image->data[i].Green;
      ret->data[i].Blue = image->data[i].Blue;
      ret->data[i].Reserved = image->data[i].Reserved;
      i++;
    }
  // TODO : FIND WHY THE FUCK I NEED TO DO THIS
  i = 0;
  return (ret);
}

t_img                           *resize_image(t_img *img, UINTN height, UINTN width)
{
  t_img                         *ret;
  UINTN                         i;
  UINTN                         j;
  UINTN                         x_ratio, y_ratio, x_diff, y_diff;
  UINTN                         offset, index, x, y;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL a, b, c, d;


  if (img->height == height && img->width == width)
    return (img);
  j = 0;
  offset = 0;
  ret = init_image(height, width);
  x_ratio = ((img->width - 1) * SCALE_MULTIPLIER) / width;
  y_ratio = ((img->height - 1) * SCALE_MULTIPLIER) / height;
  while (i < height)
    {
      j = 0;
      while (j < width)
        {
          x = (j * (img->width - 1)) / width;
          y = (i * (img->height - 1)) / height;
          x_diff = (x_ratio * j) - x * SCALE_MULTIPLIER;
          y_diff = (y_ratio * i) - y * SCALE_MULTIPLIER;
          x_diff = (x_ratio * j) - x * SCALE_MULTIPLIER;
          y_diff = (y_ratio * i) - y * SCALE_MULTIPLIER;
          index = ((y * img->width) + x);
          a = img->data[index];
          b = img->data[index + 1];
          c = img->data[index + img->width];
          d = img->data[index + img->width + 1];
          ret->data[offset].Blue = ((a.Blue) * (SCALE_MULTIPLIER - x_diff) * (SCALE_MULTIPLIER - y_diff) +
                                    (b.Blue) * x_diff * (SCALE_MULTIPLIER - y_diff) +
                                    (c.Blue) * y_diff * (SCALE_MULTIPLIER - x_diff) +
                                    (d.Blue) * (x_diff * y_diff)) / (SCALE_MULTIPLIER * SCALE_MULTIPLIER);
          ret->data[offset].Red = ((a.Red) * (SCALE_MULTIPLIER - x_diff) * (SCALE_MULTIPLIER - y_diff) +
                                   (b.Red) * x_diff * (SCALE_MULTIPLIER - y_diff) +
                                   (c.Red) * y_diff * (SCALE_MULTIPLIER - x_diff) +
                                   (d.Red) * (x_diff * y_diff)) / (SCALE_MULTIPLIER * SCALE_MULTIPLIER);
          ret->data[offset].Green = ((a.Green) * (SCALE_MULTIPLIER - x_diff) * (SCALE_MULTIPLIER - y_diff) +
                                     (b.Green) * x_diff * (SCALE_MULTIPLIER - y_diff) +
                                     (c.Green) * y_diff * (SCALE_MULTIPLIER - x_diff) +
                                     (d.Green) * (x_diff * y_diff)) / (SCALE_MULTIPLIER * SCALE_MULTIPLIER);
          ret->data[offset].Reserved = ((a.Reserved) * (SCALE_MULTIPLIER - x_diff) * (SCALE_MULTIPLIER - y_diff) +
                                        (b.Reserved) * x_diff * (SCALE_MULTIPLIER - y_diff) +
                                        (c.Reserved) * y_diff * (SCALE_MULTIPLIER - x_diff) +
                                        (d.Reserved) * (x_diff * y_diff)) / (SCALE_MULTIPLIER * SCALE_MULTIPLIER);
          offset++;
          j++;
        }
      i++;
    }
  free_image(img);
  return (ret);
}

UINT8		get_color_alpha_blended(UINT8 bg, UINT8 fg, UINT8 alpha)
{
  UINT8		ret;
  UINT8		inv_alpha;

  inv_alpha = 255 - alpha;
  ret = (alpha * fg + inv_alpha * bg) / 255;
  return (ret);
}

void            draw_image_at(UINTN startx, UINTN starty, t_graphics *screen, t_img *image)
{
  UINTN         screen_x;
  UINTN         screen_y;
  UINTN         img_x;
  UINTN         img_y;
  UINTN		screen_pos;
  UINTN		img_pos;

  screen_y = starty;
  img_y = 0;
  while (img_y < image->height && screen_y < screen->height)
    {
      img_x = 0;
      screen_x = startx;
      while (img_x < image->width && screen_x < screen->width)
        {
          // for an effect of old image -> image->data[img_x * img_y]
	  img_pos = img_y * image->width + img_x;
	  screen_pos = screen_y * screen->width + screen_x;
          /*if (image->data[img_pos].Reserved <= 100)
	  {
              screen->pixels[screen_pos].Red = image->data[img_pos].Red;
              screen->pixels[screen_pos].Blue = image->data[img_pos].Blue;
              screen->pixels[screen_pos].Green = image->data[img_pos].Green;
              screen->pixels[screen_pos].Reserved = 0;
	      }*/
	  screen->pixels[screen_pos].Red = get_color_alpha_blended(screen->pixels[screen_pos].Red, image->data[img_pos].Red, image->data[img_pos].Reserved);
	  screen->pixels[screen_pos].Blue = get_color_alpha_blended(screen->pixels[screen_pos].Blue, image->data[img_pos].Blue, image->data[img_pos].Reserved);
	  screen->pixels[screen_pos].Green = get_color_alpha_blended(screen->pixels[screen_pos].Green, image->data[img_pos].Green, image->data[img_pos].Reserved);
	  screen->pixels[screen_pos].Reserved = 255;
          img_x++;
          screen_x++;
        }
      img_y++;
      screen_y++;
    }
}

t_img			*copy_pixels_zone(UINTN startx, UINTN starty, UINTN width, UINTN height, t_graphics *screen)
{
  t_img			*ret;
  UINTN			curr_x;
  UINTN			curr_y;
  UINTN			i;

  ret = init_image(height, width);
  curr_y = starty;
  i = 0;
  while (curr_y < starty + height)
    {
      curr_x = startx;
      while (curr_x < startx + width)
	{
	  ret->data[i].Red = screen->pixels[screen->width * curr_y + curr_x].Red;
	  ret->data[i].Blue = screen->pixels[screen->width * curr_y + curr_x].Blue;
	  ret->data[i].Green = screen->pixels[screen->width * curr_y + curr_x].Green;
	  ret->data[i].Reserved = screen->pixels[screen->width * curr_y + curr_x].Reserved;
	  curr_x++;
	  i++;
	}
      curr_y++;
    }
  return (ret);
}
