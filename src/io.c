/*
** io.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Wed Apr 19 08:04:28 2017 grandr_r
** Last update Thu Apr 27 10:58:20 2017 grandr_r
*/

#include <Myre.h>

UINT8		*message_to_buffer(CHAR16 *message)
{
  UINT8		i;
  UINT8		*ret;
  UINTN		len;

  i = 0;
  len = strlen(message);
  ret = AllocateZeroPool(sizeof(UINT8) * (2 + len));
  while (i < len)
    {
      ret[i] = (UINT8)message[i];
      i++;
    }
  return (ret);
}

void		log(t_env *env, CHAR16 *message)
{
  UINT8			*buffer;
  UINTN			buffer_size;

  if (env->log_file == NULL)
    return;
  buffer_size = strlen(message);
  buffer = message_to_buffer(message);
  env->log_file->Write(env->log_file, &buffer_size, buffer);
  FreePool(buffer);
}
