/*
** load_bmp.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Wed Mar 22 08:44:54 2017 grandr_r
** Last update Thu Mar 23 08:52:26 2017 grandr_r
*/

#include <Myre.h>

char	is_in_arr(UINT8 *arr, UINT8 val)
{
  int	i;

  i = 0;
  while (arr[i])
    {
      if (arr[i] == val)
	return (1);
      i++;
    }
  return (0);
}

t_bmp	*verify_header(t_file *file, UINT8 *arr)
{
  t_bmp	*header;

  if (file->size < sizeof(t_bmp) || file->buffer == NULL)
    return (NULL);
  header = (t_bmp *)file->buffer;
  Print(L"compression_type = %d\r\nheader->bpp = %d\r\n", header->compression_type, header->bit_per_pixel);
  if (header->b != 'B' || header->m != 'M')
    return (NULL);
  if (header->compression_type != 0)
    return (NULL);
  if (!is_in_arr(arr, header->bit_per_pixel))
    return (NULL);
  return (header);
}

t_img				*bmp1(t_file *file, t_bmp *header)
{
  UINTN				offset;
  t_img 			*ret;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*all_img;
  UINT8				*base_ptr;
  UINT8				*ptr;
  UINTN				x;
  UINTN				y;
  UINTN				index;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*current_pixel;
  UINT8				value;
  UINTN				bit_index;

  offset = (header->pixel_width + 7) >> 3;
  if (offset % 4 != 0)
    offset = offset + (4 - offset % 4);
  if ((header->image_offset + offset * header->pixel_width) > file->size)
    return (NULL);
  ret = init_image(header->pixel_height, header->pixel_width);
  all_img = (EFI_GRAPHICS_OUTPUT_BLT_PIXEL *)(file->buffer + sizeof(*header));
  y = 0;
  base_ptr = file->buffer + header->image_offset;
  while (y < header->pixel_height)
    {
      x = 0;
      ptr = base_ptr;
      base_ptr += offset;
      current_pixel = ret->data + (header->pixel_height - 1 - y) * header->pixel_width;
      while (x < header->pixel_width)
	{
	  bit_index = x & 0x07;
	  if (bit_index == 0)
	    value = *ptr++;
	  index = (value >> (7 - bit_index)) & 0x01;
	  current_pixel->Red = all_img[index].Red;
	  current_pixel->Green = all_img[index].Green;
	  current_pixel->Blue = all_img[index].Blue;
	  current_pixel->Reserved = 255;
	  current_pixel++;
	  x++;
	}
      y++;
    }
  return (ret);
}

t_img	*bmp4(t_file *file, t_bmp *header)
{
  UINTN				offset;
  t_img 			*ret;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*all_img;
  UINT8				*base_ptr;
  UINT8				*ptr;
  UINTN				x;
  UINTN				y;
  UINTN				index;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*current_pixel;
  UINT8				value;

  offset = (header->pixel_width + 1) >> 1;
  if (offset % 4 != 0)
    offset = offset + (4 - offset % 4);
  if ((header->image_offset + offset * header->pixel_width) > file->size)
    return (NULL);
  ret = init_image(header->pixel_height, header->pixel_width);
  all_img = (EFI_GRAPHICS_OUTPUT_BLT_PIXEL *)(file->buffer + sizeof(*header));
  y = 0;
  base_ptr = file->buffer + header->image_offset;
  while (y < header->pixel_height)
    {
      x = 0;
      ptr = base_ptr;
      base_ptr += offset;
      current_pixel = ret->data + (header->pixel_height - 1 - y) * header->pixel_width;
      while (x < header->pixel_width)
	{
	  value = *ptr++;
	  index = (value >> 4);
	  current_pixel->Red = all_img[index].Red;
	  current_pixel->Green = all_img[index].Green;
	  current_pixel->Blue = all_img[index].Blue;
	  current_pixel->Reserved = 255;
	  current_pixel++;
	  x++;
	}
      y++;
    }
  return (ret);
}

t_img	*bmp8(t_file *file, t_bmp *header)
{
  UINTN				offset;
  t_img 			*ret;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*all_img;
  UINT8				*base_ptr;
  UINT8				*ptr;
  UINTN				x;
  UINTN				y;
  UINTN				index;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*current_pixel;

  offset = (header->pixel_width + 7) >> 3;
  if (offset % 4 != 0)
    offset = offset + (4 - offset % 4);
  if ((header->image_offset + offset * header->pixel_width) > file->size)
    return (NULL);
  ret = init_image(header->pixel_height, header->pixel_width);
  all_img = (EFI_GRAPHICS_OUTPUT_BLT_PIXEL *)(file->buffer + sizeof(*header));
  y = 0;
  base_ptr = file->buffer + header->image_offset;
  while (y < header->pixel_height)
    {
      x = 0;
      ptr = base_ptr;
      base_ptr += offset;
      current_pixel = ret->data + (header->pixel_height - 1 - y) * header->pixel_width;
      while (x < header->pixel_width)
	{
	  index = *ptr++;
	  current_pixel->Red = all_img[index].Red;
	  current_pixel->Green = all_img[index].Green;
	  current_pixel->Blue = all_img[index].Blue;
	  current_pixel->Reserved = 255;
	  current_pixel++;
	  x++;
	}
      y++;
    }
  return (ret);
}

t_img	*bmp24(t_file *file, t_bmp *header)
{
  UINTN				offset;
  t_img 			*ret;
  UINT8				*base_ptr;
  UINT8				*ptr;
  UINTN				x;
  UINTN				y;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL	*current_pixel;

  offset = header->pixel_width * 3;
  if (offset % 4 != 0)
    offset = offset + (4 - offset % 4);
  if ((header->image_offset + offset * header->pixel_height) > file->size)
    return (NULL);
  ret = init_image(header->pixel_height, header->pixel_width);
  y = 0;
  base_ptr = file->buffer + header->image_offset;
  while (y < header->pixel_height)
    {
      x = 0;
      ptr = base_ptr;
      base_ptr += offset;
      current_pixel = ret->data + (header->pixel_height - 1 - y) * header->pixel_width;
      while (x < header->pixel_width)
	{
	  current_pixel->Blue = *ptr++;
	  current_pixel->Green = *ptr++;
	  current_pixel->Red = *ptr++;
	  current_pixel->Reserved = 255;
	  current_pixel++;
	  x++;
	}
      y++;
    }
  return (ret);
}

void	init_arr_and_bmp_ptr(UINT8 *arr, t_img *(**bmp_ptr)(t_file *, t_bmp *))
{
  arr[0] = 1;
  arr[1] = 4;
  arr[2] = 8;
  arr[3] = 24;
  bmp_ptr[0] = bmp1;
  bmp_ptr[1] = bmp4;
  bmp_ptr[2] = bmp8;
  bmp_ptr[3] = bmp24;
}

t_img	*read_bmp(t_file *file)
{
  t_img	*ret;
  UINT8	arr[4];
  t_img	*(*bmp_ptr[4])(t_file *, t_bmp *);
  int	i;
  t_bmp	*header;

  i = 0;
  init_arr_and_bmp_ptr(arr, bmp_ptr);
  if (!(header = verify_header(file, arr)))
    return (NULL);
  Print(L"header ok\r\n");
  while (i < 4)
    {
      if (header->bit_per_pixel == arr[i])
	ret = bmp_ptr[i](file, header);
      i++;
    }
  return (ret);
}
