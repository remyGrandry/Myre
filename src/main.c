/*
** main.c for  in /home/grandr_r/efi/Myre
**
** Made by grandr_r
** Login   <grandr_r@padawan.ru>
**
** Started on  Thu Feb 16 09:30:41 2017 grandr_r
** Last update Thu Apr 27 14:06:47 2017 grandr_r
*/

#include <Myre.h>

t_env			*init_all(EFI_HANDLE image, EFI_SYSTEM_TABLE *sys_table, CHAR16 **mode_from_env)
{
  t_env			*env;

  InitializeLib(image, sys_table);
  BS->SetWatchdogTimer(0, 0, 0, NULL);
  env = init_env(image);
  *mode_from_env = getenv_cmd(env, L"MODE=");
  log(env, L"Choosen mode for this instance : ");
  return (env);
}

EFI_STATUS		mode_wrapper(t_env *env, CHAR16 *mode_from_env)
{
  UINT8			i;
  EFI_STATUS		err;
  CHAR16		*modes[4] = {L"shell", L"graphics", L"list", NULL};
  EFI_STATUS		(*selected_mode[5])(t_env *env) = {shell_loop, graphics_loop, list_loop, shell_loop, NULL};

  i = 0;
  while (strcmp(modes[i], mode_from_env) != 0 && modes[i])
    i++;
  if (i >= 3)
    {
      log(env, L"Error -> Shell");
      add_to_env(env, L"ERROR_MODE=true");
    }
  else
    log(env, getenv_cmd(env, L"MODE="));
  err = selected_mode[i](env);
  return (err);
}

EFI_STATUS		efi_main(EFI_HANDLE image, EFI_SYSTEM_TABLE *sys_table)
{
  EFI_STATUS		err;
  t_env			*env;
  CHAR16		*mode_from_env;

  mode_from_env = NULL;
  env = init_all(image, sys_table, &mode_from_env);
  err = mode_wrapper(env, mode_from_env);
  free_env(env);
  BS->Exit(image, err, 0, NULL);
  /* TODO EXIT BOOTSERVICES */
  //BS->ExitBootServices(image, BS->GetMemoryMap());
  return (err);
}
